package com.example.checkout.repository;

import com.example.checkout.model.Prices;
import org.springframework.data.repository.CrudRepository;

public interface CheckoutRepository extends CrudRepository<Prices, Long> {
}
