package com.example.checkout.service.impl;

import com.deliveredtechnologies.rulebook.FactMap;
import com.deliveredtechnologies.rulebook.NameValueReferableMap;
import com.deliveredtechnologies.rulebook.lang.RuleBookBuilder;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.example.checkout.business.CheckoutRules;
import com.example.checkout.dto.PriceDto;
import com.example.checkout.model.Prices;
import com.example.checkout.model.adapter.PricesAdapter;
import com.example.checkout.repository.CheckoutRepository;
import com.example.checkout.service.ICheckoutService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class CheckoutServiceImpl implements ICheckoutService {

    private List<String> codeProducts;
    private CheckoutRules checkoutRules;
    private CheckoutRepository checkoutRepository;


    @Override
    public void addNewProduct() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("New product: ");
        String newProd = scanner.nextLine();
        System.out.println("Product " + newProd + " add into cart list");
        codeProducts.add(newProd);

    }

    @Override
    public List<PriceDto> getPrices() {

        List<Prices> pricesList = StreamSupport.stream(checkoutRepository.findAll().spliterator(),false)
                .collect(Collectors.toList());

        return PricesAdapter.toPriceDtoList(pricesList);
    }

    @Override
    public void updateCheckoutRules() {

        List<PriceDto> priceDtoList = getPrices();

        BigDecimal totPriceStores = getPriceProduct(priceDtoList, "VOUCHER")
                .add(getPriceProduct(priceDtoList, "TSHIRT"))
                .add(getPriceProduct(priceDtoList, "PANTS"));

        RuleBook storesPriceRuleBook = RuleBookBuilder.create(CheckoutRules.class)
                .withResultType(BigDecimal.class)
                .withDefaultResult(totPriceStores)
                .build();

        NameValueReferableMap facts = new FactMap();
        facts.setValue("VOUCHER", getPriceProduct(priceDtoList, "VOUCHER"));
        facts.setValue("TSHIRT", getPriceProduct(priceDtoList, "TSHIRT"));
        facts.setValue("PANTS",  getPriceProduct(priceDtoList, "PANTS"));

        storesPriceRuleBook.run(facts);

        storesPriceRuleBook.getResult()
                .ifPresent(result ->
                        System.out.println("Applicant discounts for the following price: " + result));
    }

    private BigDecimal getPriceProduct(List<PriceDto> priceDtoList, String codeProduct) {

        BigDecimal productPrice = priceDtoList.stream()
                .filter(priceDto -> priceDto.getCode().equals(codeProduct))
                .map(PriceDto::getPrice)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        Long productCounter = codeProducts.stream()
                .filter(priceDto -> priceDto.equals(codeProduct))
                .count();

        return productPrice.multiply(BigDecimal.valueOf(productCounter));

    }


}
