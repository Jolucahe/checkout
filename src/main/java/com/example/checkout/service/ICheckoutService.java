package com.example.checkout.service;

import com.example.checkout.dto.PriceDto;

import java.math.BigDecimal;
import java.util.List;

public interface ICheckoutService {

    void addNewProduct();
    List<PriceDto> getPrices();
    void updateCheckoutRules();

}
