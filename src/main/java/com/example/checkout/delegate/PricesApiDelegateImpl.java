package com.example.checkout.delegate;

import com.example.checkout.api.PricesApiDelegate;
import com.example.checkout.dto.PriceDto;
import com.example.checkout.service.ICheckoutService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class PricesApiDelegateImpl implements PricesApiDelegate {

    private ICheckoutService checkoutService;

    @Override
    public ResponseEntity<List<PriceDto>> getPrices() {
        return ResponseEntity.ok(checkoutService.getPrices());
    }

    @Override
    public ResponseEntity<Void> createNewProduct() {
        checkoutService.addNewProduct();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Void> getPricesDiscounts() {
        checkoutService.updateCheckoutRules();
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
