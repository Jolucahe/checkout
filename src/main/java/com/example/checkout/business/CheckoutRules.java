package com.example.checkout.business;

import com.deliveredtechnologies.rulebook.lang.RuleBuilder;
import com.deliveredtechnologies.rulebook.model.rulechain.cor.CoRRuleBook;

import java.math.BigDecimal;
import java.util.List;

public class CheckoutRules extends CoRRuleBook<BigDecimal> {
    @Override
    public void defineRules() {


        addRule(RuleBuilder.create()
                .withFactType(List.class)
                .withResultType(BigDecimal.class)
                .when(facts -> facts.values()
                        .stream()
                        .filter(item -> item.getValue().equals("VOUCHER"))
                        .count() >= 2)
                .then((facts, result) -> result.setValue(result.getValue().subtract(BigDecimal.valueOf(5))))
                .build());

        addRule(RuleBuilder.create()
                .withFactType(List.class)
                .withResultType(BigDecimal.class)
                .when(facts -> facts.values()
                        .stream()
                        .filter(item -> item.getValue().equals("TSHIRT"))
                        .count() >= 3)
                .then((facts, result) ->
                        result.setValue(
                                result.getValue()
                                        .subtract(BigDecimal.valueOf(
                                                facts.values()
                                                        .stream()
                                                        .filter(item -> item.getValue().equals("TSHIRT"))
                                                        .count()
                                        )))
                )
                .build());

    }
}
