package com.example.checkout.model.adapter;

import com.example.checkout.dto.PriceDto;
import com.example.checkout.model.Prices;

import java.util.List;
import java.util.stream.Collectors;

public class PricesAdapter {

    public static List<PriceDto> toPriceDtoList(List<Prices> pricesList) {

        return pricesList.stream()
                .map(prices -> {PriceDto priceDto = new PriceDto();
                    priceDto.setCode(prices.getCode());
                    priceDto.setName(prices.getName());
                    priceDto.setPrice(prices.getPrice());
                    return priceDto;})
                .collect(Collectors.toList());
    }
}
